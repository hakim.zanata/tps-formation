## TP Linux

### Linux:
tout d'abord, on doit ajouter  les jobs dans la liste des cron jobs par la commande : 
```bash
crontab -e
```
```bash
centos👽machine:~/tps_formation$ crontab -l
5  0  *  *  *  ~/linux/consommationCT.sh midnight
5  *  *  *  *  ~/linux/consommationCT.sh 5min
0  2  5,15,19,25,29  *  *  ~/linux/consommationCT.sh 2h
15  20  1  *  *  ~/linux/consommationCT.sh  frstmon
30  23  *  *  mon-fri  ~/linux/consommationCT.sh week

```
on a planifié des taches :

*  ***midnight : chaque jour à 00:05***
* ***5min : chaque heure à la cinquième minute***
* ***2hmont: 02h00 tous les 5, 15, 19, 25 et 29 de chaque mois***
* ***frstmon:  20h15 le premier de chaque mois***
* ***week :23:30 uniquement en semaine (excluez le samedi et le dimanche)***
le script prend un argument ***$1*** 
dans le script, on a utilisé un dictionnaire pour simplifier les choses et aussi une procédure pour gérer les erreurs dans tue le script totalement

#### *`consommationCT.sh`*
```bash
#!/usr/bin/bash

#declaration d'un dictionnaire temps
declare -A temps=(
  ["midnight"]="minuit tous les jours"
  ["5min"]="5ème min tous les heurs"
  ["2h"]="à 2:00 tous les 5,15,19,25,29 de chaque mois"
  ["frstmon"]="à 20:15 du premier jour de chaque mois"
  ["week"]="à 23:30 du lundi au vendredi"
)
#le condition verifie si l'argument est dans la liste des cles
if [ -v temps[$1] ]; then
  echo "|`date +'%H:%M:%S %d-%m'`|[JOB] ${temps[$1]}" >> consommationCT.log
else
  echo "|`date +'%H:%M:%S %d-%m'`|[Error]" >> consommationCT.log
```
le fichier log sera enregistrer dans ``` ~/ ```
le fichier log:
```bash 
centos👽machine:~$ cat consommationCT.log 
|10:05:01 16-08|[JOB] 5ème min tous les heurs
|11:05:01 16-08|[JOB] 5ème min tous les heurs
|12:05:01 16-08|[JOB] 5ème min tous les heurs
|13:05:02 16-08|[JOB] 5ème min tous les heurs
|14:05:01 16-08|[JOB] 5ème min tous les heurs
``` 
#### script et service:
Créer un script ```eligibilite.sh``` qui affiche indéfiniment le message "client éligible".

#### *`eligibilite.sh`*
```bash
#!/usr/bin/bash

while true; do
  echo "client éligible"
  sleep 1
done
```
le service qui gère le fichier est placé a `/etc/systemd/system` le fichier `eligibilite.service`⁣ :
#### `eligibilite.service`
```bash
[Unit]
Description=eligibilite service

[Service]
ExecStart=/bin/bash /home/centos/tps_formation/eligibilite.sh 
[Install]
WantedBy=multi-user.target
```
pour lancer la service, on utilise :

```console
centos👽machine:~$ sudo systemctl start eligibilite.service
```
ou

```console
centos👽machine:~$ sudo service eligibilite.service start
```


on vérifie si le service est bien active :
```console
centos👽machine:~$ sudo systemctl status eligibilite.service 
[sudo] password for centos: 
● eligibilite.service - eligibilite service
   Loaded: loaded (/etc/systemd/system/eligibilite.service; disabled; vendor >
   Active: active (running) since Tue 2022-08-16 10:41:27 EDT; 5h 10min ago
 Main PID: 6503 (bash)
    Tasks: 2 (limit: 11416)
   Memory: 596.0K
   CGroup: /system.slice/eligibilite.service
           ├─ 6503 /bin/bash /home/centos/tps_formation/eligibilite.sh
           └─42216 sleep 1

Aug 16 15:51:35 machine bash[6503]: client éligible
Aug 16 15:51:36 machine bash[6503]: client éligible
Aug 16 15:51:37 machine bash[6503]: client éligible
Aug 16 15:51:38 machine bash[6503]: client éligible
Aug 16 15:51:39 machine bash[6503]: client éligible
Aug 16 15:51:40 machine bash[6503]: client éligible
Aug 16 15:51:41 machine bash[6503]: client éligible
Aug 16 15:51:42 machine bash[6503]: client éligible
Aug 16 15:51:43 machine bash[6503]: client éligible
Aug 16 15:51:44 machine bash[6503]: client éligible
centos👽machine:~$ 
```
### Bash:
1)
les conditions 
-   ***très bien*** si la note est entre 16 et 20 ;
-   ***bien*** lorsqu'elle est entre 14 et 16 ;
-   ***assez bien*** si la note est entre 10 et 14 ;
-   ***insuffisant***  si la note est inférieure à 10.
 #### `notes.sh`
```bash
#!/bin/bash

read -p "note: " var

if [[ $var -ge 16 && $var -le 20 ]]; then
  echo "très bien"
elif [[ $var -ge 14 && $var -lt 16 ]]; then
  echo "bien"
elif [[ $var -ge 10 && $var -lt 14 ]]; then
  echo "assez bien"
else
  echo "insuffisant"
fi
```

2)
 #### `existe.sh`
```bash
#!/bin/bash

if [[ -e $1 ]]; then
  echo "$1 existe"
fi
```
-e : vrai si l'argument est un fichier, dossier, lien symbolique... on peut remplacer le script précédent par celui-là qui vérifie juste les répertoires et fichiers :
 #### `existe.sh`
```bash
#!/bin/bash

if [[ -d $1 || -f $1 ]]; then
  echo "$1 existe"
fi
```
 3)
 le script qui calcule le nombre de dossiers, fichiers et fichiers exécutables :
 #### `file_dir_counter.sh`
 ```bash
#!/bin/bash

echo "nombre de fichers: `find "$1" -type f | wc -l`"
echo "nombre de dossiers: `find "$1" -maxdepth 1 -type d | wc -l`"
echo "nombre de fichiers executables: `find "$1" -type f -executable | wc -l`"
 ```
 il faut exécuter avec le droit sudo le resultat sera:
```console
centos👽machine:~/tps_formation$ sudo ./file_dir_counter.sh /etc
nombre de fichers: 523
nombre de dossiers: 94
nombre de fichiers executables: 23
centos👽machine:~/tps_formation$ 
 ```
 
