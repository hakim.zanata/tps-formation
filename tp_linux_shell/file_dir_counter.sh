#"!/bin/bash

echo "nombre de fichers: `find "$1" -type f | wc -l`"
echo "nombre de dossiers: `find "$1" -maxdepth 1 -type d | wc -l`"
echo "nombre de fichiers executables: `find "$1" -type f -executable | wc -l`"
