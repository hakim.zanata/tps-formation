#!/usr/bin/bash

declare -A temps=(
  ["midnight"]="minuit tous les jours"
  ["5min"]="5ème min tout les heurs"
  ["2h"]="à 2:00 tous les 5,15,19,25,29 de chaque mois"
  ["frstmon"]="à 20:15 du premier jour de chaque mois"
  ["week"]="à 23:30 du lundi au vendredi"
)

if [ -v temps[$1] ]; then
  echo "|`date +'%H:%M:%S %d-%m'`|[JOB] ${temps[$1]}" >> ./consommationCT.log
else
  echo "|`date +'%H:%M:%S %d-%m'`|[Error]" >> ./consommationCT.log
fi
