#!/bin/bash

read -p "note: " var

if [[ $var -ge 16 && $var -le 20 ]]; then
  echo "très bien"
elif [[ $var -ge 14 && $var -lt 16 ]]; then
  echo "bien"
elif [[ $var -ge 10 && $var -lt 14 ]]; then
  echo "assez bien"
else
  echo "insuffisant"
fi
