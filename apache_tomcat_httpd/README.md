# Apache HTTP server
Après se connecter à la VM, on installe et configure le serveur HTTP:
- installation: 
```bash
[hakim@instance-5 ~]$ sudo dnf install httpd -y 
```
- démarrer le service associe par :
```bash
[hakim@instance-5 ~]$ sudo systemctl enable --now httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[hakim@instance-5 ~]$ 
```
Ce qui permet de démarrer et activer httpd
- creation index.html:
```html
<html>
        <head>
                <title>new http server</title>
        </head>
        <body>
                <h2>
                        Hello, World!
                </h2>
        </body> 
</html>
```
- Activer le port HTTP(80) et HTTPS(443) dans le firewall .
```bash
 [hakim@instance-5 ~]$ sudo firewall-cmd --permanent --zone=public --add-service={http,https}
success
[hakim@instance-5 ~]$ sudo firewall-cmd --reload
```
- Tester si le serveur fonctionne : puisqu'on est connecté à une machine dans le cloud, on doit récupérer l'IP extérieur par :
```bash
[hakim@instance-5 ~]$ curl ifconfig.me; echo
34.163.58.68
[hakim@instance-5 ~]$ curl http://$(curl ifconfig.me)
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    12  100    12    0     0    112      0 --:--:-- --:--:-- --:--:--   112
<html>
        <head>
                <title>new http server</title>
        </head>
        <body>
                <h2>
                        Hello, World!
                </h2>
        </body> 
</html>
[hakim@instance-5 ~]$  
```
On se déplace vers le home de l'utilisateur apache et crée un dossier pour l'hôte goldendevops :
```bash
[hakim@instance-5 ~]$ cd /var/www/
[hakim@instance-5 www]$ ll
total 0
drwxr-xr-x. 2 root root  6 Jun 15 12:29 cgi-bin
drwxr-xr-x. 2 root root 24 Sep 14 17:33 html
[hakim@instance-5 www]$ sudo mkdir -p  www.goldendevops.com/{html,log}
[hakim@instance-5 www]$ sudo dnf install -y tree 1>/dev/null
[hakim@instance-5 www]$ tree
.
├── cgi-bin
├── html
│   └── index.html
└── www.goldendevops.com
    ├── html
    └── log

5 directories, 1 file
[hakim@instance-5 www]$ sudo chown -R apache:apache www.goldendevops.com
[hakim@instance-5 www]$ sudo vim www.goldendevops.com/html/index.html
[hakim@instance-5 www]$ cat www.goldendevops.com/html/index.html 
<html>
        <head>
           <meta charset="utf-8">
             <title>Bienvenue à www.goldendevops.com </title>
       </head>
          <body>
               <h1>Portail goldendevops sécurisé !</h1>
          </body>
</html>
[hakim@instance-5 www]$
```
On ajoute l'IP de la machine à notre DNS local dans /etc/hosts
```bash
╭─hakim🐺imap in ~ 
╰$ tail -n1 /etc/hosts          
34.163.58.68	www.goldendevops.com	goldendevops.com
╭─hakim🐺imap in ~ 
╰$ curl -i www.goldendevops.com
HTTP/1.1 200 OK
Date: Wed, 14 Sep 2022 20:13:20 GMT
Server: Apache/2.4.37 (Red Hat Enterprise Linux)
Last-Modified: Wed, 14 Sep 2022 18:16:30 GMT
ETag: "eb-5e8a720ca61b5"
Accept-Ranges: bytes
Content-Length: 235
Content-Type: text/html; charset=UTF-8

<html>
        <head>
           <meta charset="utf-8">
             <title>Bienvenue à www.goldendevops.com </title>
       </head>
          <body>
               <h1>Portail goldendevops sécurisé !</h1>
          </body>
</html>
╭─hakim🐺imap in ~ 
╰$ 
╭─hakim🐺imap in ~ 
╰$ tail -n1 /etc/hosts      
34.163.58.68	www.goldendevops.com	goldendevops.com
╭─hakim🐺imap in ~ 
╰$ 
```
## *Configuration certificate SSL*:
En utilisant *OpenSSL*, on crée des certificats SSL par :
```console
[root@instance-5 ~]# openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/goldendevops.key -out /etc/ssl/certs/goldendevops.crt
Generating a RSA private key
.....................................................................................................................+++++
...........+++++
writing new private key to '/etc/ssl/private/goldendevops.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:
State or Province Name (full name) []:
Locality Name (eg, city) [Default City]:
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:goldendevops.com
Email Address []:
[root@instance-5 ~]# tree /etc/ssl/private/ /etc/ssl/certs/
/etc/ssl/private/
└── goldendevops.key
/etc/ssl/certs/
├── ca-bundle.crt -> /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
├── ca-bundle.trust.crt -> /etc/pki/ca-trust/extracted/openssl/ca-bundle.trust.crt
└── goldendevops.crt

0 directories, 4 files
[root@instance-5 ~]# 

```
On crée un autre fichier de configuration dans /etc/httpd/sites-avalaible :
```console
[root@instance-5 httpd]# pwd
/etc/httpd
[root@instance-5 httpd]# cat sites-available/goldendevops.https.conf 
<VirtualHost *:443>
    ServerAdmin hakim.oubouali.skr@gmail.com
    ServerName goldendevops.com
    ServerAlias www.goldendevops.com
    DocumentRoot /var/www/goldendevops.com/html
    <Directory /var/www/goldendevops.com/html>
        Options -Indexes +FollowSymLinks
        AllowOverride All
    </Directory>
    ErrorLog    /var/www/goldendevops.com/log/error.log
    CustomLog /var/www/goldendevops.com/log/access.log combined
    SSLEngine on
    SSLCertificateFile /etc/ssl/certs/goldendevops.crt
    SSLCertificateKeyFile /etc/ssl/private/goldendevops.key
</VirtualHost>
[root@instance-5 httpd]# ln -s /etc/httpd/sites-available/goldendevops.https.conf /etc/httpd/sites-enabled/goldendevops.https.conf
[root@instance-5 httpd]# sudo systemctl restart httpd
[root@instance-5 httpd]# curl -i https://goldendevops.com
HTTP/1.1 200 OK
Date: Wed, 14 Sep 2022 20:54:54 GMT
Server: Apache/2.4.37 (Red Hat Enterprise Linux) OpenSSL/1.1.1k
Last-Modified: Wed, 14 Sep 2022 18:16:30 GMT
ETag: "eb-5e8a720ca61b5"
Accept-Ranges: bytes
Content-Length: 235
Content-Type: text/html; charset=UTF-8

<html>
        <head>
           <meta charset="utf-8">
             <title>Bienvenue à www.goldendevops.com </title>
       </head>
          <body>
               <h1>Portail goldendevops sécurisé !</h1>
          </body>
</html>
[root@instance-5 httpd]#
```

# Apache Tomcat:

On se connecte une autre machine VM et on crée un dossier tomcat3 :
```bash
╭─hakim🐺imap in ~ 
╰$ ssh root@139.162.185.77 
Last failed login: Wed Sep 14 21:34:54 UTC 2022 from 138.197.152.128 on ssh:notty
There were 1038 failed login attempts since the last successful login.
Last login: Wed Sep 14 13:03:52 2022 from 93.22.149.210
[root@139-162-185-77 ~]# cd /opt/
[root@139-162-185-77 opt]# ll
total 8
drwxr-xr-x. 3 tomcat tomcat 4096 Sep  9 14:53 tomcat
drwxr-xr-x. 3 tomcat tomcat 4096 Sep  9 15:09 tomcat1
[root@139-162-185-77 opt]# cp -r  tomcat tomcat2
[root@139-162-185-77 opt]# mv tomcat2 tomcat3
[root@139-162-185-77 opt]# ll
total 12
drwxr-xr-x. 3 tomcat tomcat 4096 Sep  9 14:53 tomcat
drwxr-xr-x. 3 tomcat tomcat 4096 Sep  9 15:09 tomcat1
drwxr-xr-x. 3 root   root   4096 Sep 14 21:35 tomcat3
[root@139-162-185-77 opt]# sudo chown -R tomcat:tomcat tomcat3
[root@139-162-185-77 opt]# chmod 751 tomcat3/latest/bin/*.sh
[root@139-162-185-77 opt]# 
```
 Si on veut changer le port de tomcat3, on doit modifier le fichier *server.xml*
```console
[root@139-162-185-77 latest]# sudo vim conf/server.xml 
[root@139-162-185-77 latest]# grep "port=" conf/server.xml 
<Server port="1400" shutdown="SHUTDOWN">
    <Connector port="1917" protocol="HTTP/1.1"
               port="8080" protocol="HTTP/1.1"
    <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol"
               port="8099"
[root@139-162-185-77 latest]# 

```
Après on crée un service qui gère tomcat:
```console
[root@139-162-185-77 system]# pwd
/etc/systemd/system
[root@139-162-185-77 system]# ll
total 64
drwxr-xr-x. 2 root root 4096 Sep  1 10:17  cloud-init.target.wants
lrwxrwxrwx. 1 root root   37 May  3 14:00  ctrl-alt-del.target -> /usr/lib/systemd/system/reboot.target
lrwxrwxrwx. 1 root root   41 May  3 14:00  dbus-org.fedoraproject.FirewallD1.service -> /usr/lib/systemd/system/firewalld.service
lrwxrwxrwx. 1 root root   57 May  3 14:00  dbus-org.freedesktop.nm-dispatcher.service -> /usr/lib/systemd/system/NetworkManager-dispatcher.service
lrwxrwxrwx. 1 root root   40 Sep  5 19:53  default.target -> /usr/lib/systemd/system/graphical.target
drwxr-xr-x. 2 root root 4096 May  3 14:02 'dev-virtio\x2dports-org.qemu.guest_agent.0.device.wants'
drwxr-xr-x. 2 root root 4096 May  3 14:00  getty.target.wants
drwxr-xr-x. 2 root root 4096 Sep  5 19:47  graphical.target.wants
drwxr-xr-x. 2 root root 4096 Sep  9 12:23  multi-user.target.wants
drwxr-xr-x. 2 root root 4096 May  3 14:00  network-online.target.wants
drwxr-xr-x. 2 root root 4096 Dec 21  2021  nginx.service.d
drwxr-xr-x. 2 root root 4096 May  7  2020  php-fpm.service.d
-rw-r--r--. 1 root root  660 Sep  5 21:39  prometheus.service
drwxr-xr-x. 2 root root 4096 May  3 14:00  sockets.target.wants
drwxr-xr-x. 2 root root 4096 Sep  1 10:17  sshd-keygen@.service.d
drwxr-xr-x. 2 root root 4096 May  3 14:00  sysinit.target.wants
lrwxrwxrwx. 1 root root   39 May  3 14:00  syslog.service -> /usr/lib/systemd/system/rsyslog.service
drwxr-xr-x. 2 root root 4096 May  3 14:02  sysstat.service.wants
drwxr-xr-x. 2 root root 4096 Sep  9 09:30  timers.target.wants
-rw-r--r--. 1 root root  639 Sep  9 15:21  tomcat2.service
-rw-r--r--. 1 root root  634 Sep  9 14:57  tomcat.service
[root@139-162-185-77 system]# cp tomcat2.service tomcat3.service 
[root@139-162-185-77 system]# sed -i 's/tomcat1/tomcat3/g' tomcat3.service 
[root@139-162-185-77 system]# diff tomcat3.service tomcat2.service 
11,13c11,13
< Environment="CATALINA_BASE=/opt/tomcat3/latest"
< Environment="CATALINA_HOME=/opt/tomcat3/latest"
< Environment="CATALINA_PID=/opt/tomcat3/latest/temp/tomcat.pid"
---
> Environment="CATALINA_BASE=/opt/tomcat1/latest"
> Environment="CATALINA_HOME=/opt/tomcat1/latest"
> Environment="CATALINA_PID=/opt/tomcat1/latest/temp/tomcat.pid"
15,16c15,16
< ExecStart=/bin/bash /opt/tomcat3/latest/bin/startup.sh
< ExecStop=/bin/bash /opt/tomcat3/latest/bin/shutdown.sh
---
> ExecStart=/bin/bash /opt/tomcat1/latest/bin/startup.sh
> ExecStop=/bin/bash /opt/tomcat1/latest/bin/shutdown.sh
[root@139-162-185-77 system]# 
```
On démarre le service et on verifie l'état du serveur tomcat :
```console
[root@139-162-185-77 opt]# systemctl start tomcat3
[root@139-162-185-77 opt]# systemctl status tomcat3
● tomcat3.service - Tomcat 9 servlet container
   Loaded: loaded (/etc/systemd/system/tomcat3.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2022-09-14 22:00:30 UTC; 1min 43s ago
  Process: 24844 ExecStop=/bin/bash /opt/tomcat3/latest/bin/shutdown.sh (code=exited, status=0/SUCCESS)
  Process: 24899 ExecStart=/bin/bash /opt/tomcat3/latest/bin/startup.sh (code=exited, status=0/SUCCESS)
 Main PID: 24912 (java)
    Tasks: 28 (limit: 4921)
   Memory: 279.4M
   CGroup: /system.slice/tomcat3.service
           └─24912 /usr/bin/java -Djava.util.logging.config.file=/opt/tomcat3/latest/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -Djdk.tls.ephe>

Sep 14 22:00:30 139-162-185-77.ip.linodeusercontent.com systemd[1]: Starting Tomcat 9 servlet container...
Sep 14 22:00:30 139-162-185-77.ip.linodeusercontent.com systemd[1]: Started Tomcat 9 servlet container.
[root@139-162-185-77 opt]# firewall-cmd --permanent --zone=public --add-port=1400/tcp
success
[root@139-162-185-77 opt]# firewall-cmd --reload
success
[root@139-162-185-77 opt]# netstat -tupln | grep 1400
tcp6       0      0 :::1400                 :::*                    LISTEN      24912/java          
[root@139-162-185-77 opt]# curl -I http://$(hostname):1400
HTTP/1.1 200 
Content-Type: text/html;charset=UTF-8
Transfer-Encoding: chunked
Date: Wed, 14 Sep 2022 22:02:39 GMT

[root@139-162-185-77 opt]# 
```
On a bien reçu le code status de 200 donc le serveur marche bien

si on se connecte via la console d'administration on reçoit un code status de 403 non autorise
```console
[root@139-162-185-77 opt]# curl -I http://$(hostname):1400/manager/html
HTTP/1.1 403 
Content-Type: text/html;charset=UTF-8
Transfer-Encoding: chunked
Date: Wed, 14 Sep 2022 22:06:38 GMT

[root@139-162-185-77 opt]# 
``` 
Commenter la balise *valve* et reconnecter peut résoudre le problème :
```bash
[root@139-162-185-77 opt]# vim tomcat3/latest/conf/tomcat-users.xml 
[root@139-162-185-77 opt]# grep "<user" tomcat3/latest/conf/tomcat-users.xml 
  <user username="admin" password="<must-be-changed>" roles="manager-gui"/>
  <user username="robot" password="<must-be-changed>" roles="manager-script"/>
  <user username="tomcat" password="<must-be-changed>" roles="tomcat"/>
  <user username="both" password="<must-be-changed>" roles="tomcat,role1"/>
  <user username="role1" password="<must-be-changed>" roles="role1"/>
  <user username="hakim" password="s3cret" roles="admin,admin-gui,manager,manager-gui"/>
[root@139-162-185-77 opt]# curl -I http://$(hostname):1400/manager/html -v -u hakim:s3cret
*   Trying 139.162.185.77...
* TCP_NODELAY set
* Connected to 139-162-185-77.ip.linodeusercontent.com (139.162.185.77) port 1400 (#0)
* Server auth using Basic with user 'hakim'
> HEAD /manager/html HTTP/1.1
> Host: 139-162-185-77.ip.linodeusercontent.com:1400
> Authorization: Basic aGFraW06czNjcmV0
> User-Agent: curl/7.61.1
> Accept: */*
> 
< HTTP/1.1 403 
HTTP/1.1 403 
< Cache-Control: private
Cache-Control: private
< X-Frame-Options: DENY
X-Frame-Options: DENY
< X-Content-Type-Options: nosniff
X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
X-XSS-Protection: 1; mode=block
< Content-Type: text/html;charset=UTF-8
Content-Type: text/html;charset=UTF-8
< Transfer-Encoding: chunked
Transfer-Encoding: chunked
< Date: Wed, 14 Sep 2022 22:18:35 GMT
Date: Wed, 14 Sep 2022 22:18:35 GMT

< 
* Connection #0 to host 139-162-185-77.ip.linodeusercontent.com left intact
[root@139-162-185-77 opt]# 

```
Puisque le serveur est bien connecté, on peut déployer le fichier war :
- methode 1:

![](1.png)

![](2.png)
- methode 2:
```bash
╭─hakim🐺imap in ~ 
╰$ scp Downloads/helloworld.war root@139.162.185.77:/opt/tomcat3/latest/webapps
helloworld.war                                             100%   73KB 148.5KB/s   00:00    
╭─hakim🐺imap in ~ 
╰$ curl -I http://139.162.185.77:1400/helloworld/                              
HTTP/1.1 200 
Set-Cookie: JSESSIONID=D5B2BE722DD11BFB25BF66852186063D; Path=/helloworld; HttpOnly
Content-Type: text/html;charset=UTF-8
Transfer-Encoding: chunked
Date: Wed, 14 Sep 2022 23:17:58 GMT

╭─hakim🐺imap in ~ 
╰$ 

```
On reçoit comme retour le code status 200 OK donc l'application est bien déployé
