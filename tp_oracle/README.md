# installation oracle par ansible

Ansible      | Oracle DB 21c| Spring-boot| MAVEN      | GCP
:-----------:|:-----------:|:-----------:|:-----------:|:-----------:
![absible](https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Ansible_logo.svg/256px-Ansible_logo.svg.png?20210414073712) | ![oracle DB 21c](https://futurumresearch.com/wp-content/uploads/2021/02/Oracle-Database-21c-Powering-Blockchain-and-AutoML-Innovations.jpg) | ![spring-boot](https://dz2cdn1.dzone.com/storage/temp/12434118-spring-boot-logo.png) | ![MAVEN](https://maven.apache.org/images/maven-logo-black-on-white.png) | ![GCP](https://upload.wikimedia.org/wikipedia/fr/thumb/6/61/Google_Cloud_Logo.svg/512px-Google_Cloud_Logo.svg.png?20210216111626)

# Tasks

- [x] add inventory
- [x] add playbook
- [x] create vars for prepare
- [x] create tasks for prepare
- [x] create vars for install\_db
- [x] create tasks for install\_db
- [x] configure installation template

## spécifiques

- OS Remote : Oracle Linux 8.6
- OS Local: Arch Linux 22.0.0
- ansible version: 2.13.4
- python version: 3.10.7
- méthode de virtualisation: vagrant et virtualbox comme provider

## l'inventory

inventory.yml est un fichier où on ajoute *l'Hôte* (*ip*,*clé privée*,*port* *utilisateur*)

## playbook

dans le playbook il y a deux roles :

- [x] TASK 1: preparer le système (VM 1)
  - [x] installation des dépendances
  - [x] configurer les paramètre du système
  - [x] exporter les variable d'envirenement
  - [x] copier le fichier d'installation de la template
  - [x] configurer le parfeu
  - [x] configurer Selinux
  - [x] configurer les limits pour l'utilisateur oracle
  - [x] redemarage et attent pour une connexion ssh
- [x] TASK 2: installation de la base oracle 21c (VM 1)
  - [x] demarer le fichier d'installation
  - [x] cree la base DATA
- [ ] TASK 3: preparer le système (VM 2)
  - [ ] installation des dépendances
  - [ ] configurer les paramètre du système
  - [ ] exporter les variable d'envirenement
  - [ ] configurer le parfeu
  - [ ] configurer Selinux
  - [ ] configurer les limits pour l'utilisateur adm
  - [ ] redemarage et attent pour une connexion ssh
- [ ] TASK 4: install apache HTTP server (VM 2)
- [ ] TASK 5: installation automatique de WebLogic (VM 2)
  - [ ] deployer et installer weblogic par ansible
  - [ ]
- [ ] TASK 6: REST API (VM 2)
  - [x] connecter la base Oracle avec l'app
  - [ ] cree un REST api
  - [ ] deployer dans un conteneur Docker
  - [ ] remplacer apache par nginx

### TASK 1 & 2

resultat:

```console

```

### démarrer l'API

```console
mvn spring-boot:run 
```

### api

resultat:

```bash
🏴‍☠️[api_serve] curl -X GET  http://localhost:8080/dept/4 | yq                                                                                                                             1:20:51  ☁  main ☂ ⚡ ✭
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    43    0    43    0     0   3348      0 --:--:-- --:--:-- --:--:--  3583
{
  "id": 4,
  "loc": "BOSTON",
  "name": "OPERATIONS"
}
🏴‍☠️[api_serve] curl -X GET  http://localhost:8080/dept/all | yq                                                                                                                           1:20:53  ☁  main ☂ ⚡ ✭
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   210    0   210    0     0  24607      0 --:--:-- --:--:-- --:--:-- 26250
[
  {
    "id": 1,
    "loc": "NEW YORK",
    "name": "ACCOUNTING"
  },
  {
    "id": 2,
    "loc": "DALLAS",
    "name": "RESEARCH"
  },
  {
    "id": 3,
    "loc": "CHICAGO",
    "name": "SALES"
  },
  {
    "id": 4,
    "loc": "BOSTON",
    "name": "OPERATIONS"
  },
  {
    "id": 5,
    "loc": "test123",
    "name": null
  }
]
🏴‍☠️[api_serve] curl -X POST  http://localhost:8080/dept/adddept -H 'Content-Type: application/json' -d '{"id":6,"name":"TEST","loc":"TEST123"}' | yq                                      1:20:58  ☁  main ☂ ⚡ ✭
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    74    0    36  100    38   3144   3319 --:--:-- --:--:-- --:--:--  6727
{
  "id": 6,
  "loc": "TEST123",
  "name": null
}
🏴‍☠️[api_serve] curl -X GET  http://localhost:8080/dept/5 | yq                                                                                                                             1:21:23  ☁  main ☂ ⚡ ✭
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    36    0    36    0     0   3602      0 --:--:-- --:--:-- --:--:--  4000
{
  "id": 5,
  "loc": "test123",
  "name": null
}
🏴‍☠️[api_serve] curl -X GET  http://localhost:8080/dept/6 | yq                                                                                                                             1:21:35  ☁  main ☂ ⚡ ✭
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    36    0    36    0     0   3578      0 --:--:-- --:--:-- --:--:--  4000
{
  "id": 6,
  "loc": "TEST123",
  "name": null
}
🏴‍☠️[api_serve] 
```

## google cloud platform

### creation d'un nouveau projet

on commence par cree un nouveau projet pour notre application

### creation d'un service account dans IAM & amin

![iam](./pics/iam.png)
 on cree un account service apres on le donne nome ansible
![service account]{./pics/serviceaccount.png}
 apres on donne le role: *Compute Admin*
![role](./pics/role.png)

### creation d'un playbook pour les instances

 pour savoir les images qu'on peut utulise:

 ```console
 🏴‍☠️[~] gcloud compute images list | grep rhel                                                                                                                                                             17:33:33 
 rhel-7-v20220920                                      rhel-cloud           rhel-7                                         READY
 rhel-8-v20220920                                      rhel-cloud           rhel-8                                         READY
 rhel-9-arm64-v20220920                                rhel-cloud           rhel-9-arm64                                   READY
 rhel-9-v20220920                                      rhel-cloud           rhel-9                                         READY
 rhel-7-6-sap-v20220920                                rhel-sap-cloud       rhel-7-6-sap-ha                                READY
 rhel-7-7-sap-v20220920                                rhel-sap-cloud       rhel-7-7-sap-ha                                READY
 rhel-7-9-sap-v20220920                                rhel-sap-cloud       rhel-7-9-sap-ha                                READY
 rhel-8-1-sap-v20220920                                rhel-sap-cloud       rhel-8-1-sap-ha                                READY
 rhel-8-2-sap-v20220920                                rhel-sap-cloud       rhel-8-2-sap-ha                                READY
 rhel-8-4-sap-v20220920                                rhel-sap-cloud       rhel-8-4-sap-ha                                READY
 rhel-8-6-sap-v20220920                                rhel-sap-cloud       rhel-8-6-sap-ha                                READY
 🏴‍☠️[~] 
 ```

- le recherche avant est fait sur les images publiques disponibles de rhel(Red Har Entreprise Linux)
- on peut cree notre images modifié  aui va nous répondre à nôtre besoin spécifique par ( [Packer](https://www.packer.io/) + [Vagrant](https://www.vagrantup.com/))( à faire)
- installation google-auth par pip

```bash
🏴‍☠️[deploy] pip install google-auth                                                                         18:31:12 
Defaulting to user installation because normal site-packages is not writeable
Collecting google-auth
  Downloading google_auth-2.13.0-py2.py3-none-any.whl (174 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 174.5/174.5 kB 1.1 MB/s eta 0:00:00
Requirement already satisfied: rsa<5,>=3.1.4 in /usr/lib/python3.10/site-packages (from google-auth) (4.9)
Collecting cachetools<6.0,>=2.0.0
  Downloading cachetools-5.2.0-py3-none-any.whl (9.3 kB)
Collecting pyasn1-modules>=0.2.1
  Downloading pyasn1_modules-0.2.8-py2.py3-none-any.whl (155 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 155.3/155.3 kB 837.8 kB/s eta 0:00:00
Requirement already satisfied: six>=1.9.0 in /usr/lib/python3.10/site-packages (from google-auth) (1.16.0)
Requirement already satisfied: pyasn1<0.5.0,>=0.4.6 in /usr/lib/python3.10/site-packages (from pyasn1-modules>=0.2.1->google-auth) (0.4.8)
Installing collected packages: pyasn1-modules, cachetools, google-auth
Successfully installed cachetools-5.2.0 google-auth-2.13.0 pyasn1-modules-0.2.8
🏴‍☠️[deploy]
```

### creation d'un admin server

```console
🏴‍☠️[~] gcloud compute instances create weblo --project=utopian-eye-362016 --zone=europe-west3-c --machine-type=e2-medium --network-interface=address=34.107.41.147,network-tier=PREMIUM,subnet=default --maintenance
-policy=MIGRATE --provisioning-model=STANDARD --service-account=126104345237-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --tags=http-server,https-server --create-disk=auto-delete=yes,boot=yes,device-name=weblo,image=projects/debian-cloud/global/images/debian-11-bullseye-v20220920,mode=rw,size=10,type=projects/utopian-eye-362016/zones/us-west4-b/diskTypes/pd-balanced --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any
Created [https://www.googleapis.com/compute/v1/projects/utopian-eye-362016/zones/europe-west3-c/instances/weblo].
NAME   ZONE            MACHINE_TYPE  PREEMPTIBLE  INTERNAL_IP  EXTERNAL_IP    STATUS
weblo  europe-west3-c  e2-medium                  10.156.0.5   34.107.41.147  RUNNING
🏴‍☠️[~] gcloud compute instances create oradb --project=utopian-eye-362016 --zone=europe-west3-c --machine-type=e2-medium --network-interface=address=34.159.55.61,network-tier=PREMIUM,subnet=default --maintenance-
policy=MIGRATE --provisioning-model=STANDARD --service-account=126104345237-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --create-disk=auto-delete=yes,boot=yes,device-name=oradb,image=projects/debian-cloud/global/images/debian-11-bullseye-v20220920,mode=rw,size=10,type=projects/utopian-eye-362016/zones/us-west4-b/diskTypes/pd-balanced --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any
Created [https://www.googleapis.com/compute/v1/projects/utopian-eye-362016/zones/europe-west3-c/instances/oradb].
NAME   ZONE            MACHINE_TYPE  PREEMPTIBLE  INTERNAL_IP  EXTERNAL_IP   STATUS
oradb  europe-west3-c  e2-medium                  10.156.0.6   34.159.55.61  RUNNING
🏴‍☠️[~] gcloud compute instances list                                                                                                                                                                       1:17:32 
NAME   ZONE            MACHINE_TYPE  PREEMPTIBLE  INTERNAL_IP  EXTERNAL_IP    STATUS
oradb  europe-west3-c  e2-medium                  10.156.0.6   34.159.55.61   RUNNING
weblo  europe-west3-c  e2-medium                  10.156.0.5   34.107.41.147  RUNNING
🏴‍☠️[~] 
```

on cree deux instances les deux par des ips statiques
on peut remplacer l'ip statique par cloud DNS (Bonus)
pour le serveur admin on installe openvpn pour cree un vpn dour les deux machines

### creation d'un serveur DNS (Bonus)

puisque on a cree des ip statiques pour les deux instances on ajoute un serveur dns qui nous permet la communication entre les deux instances avec un nom du domaine au lieu de l'adresse ip et ce qui nous donne un
