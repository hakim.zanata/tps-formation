#!/usr/bin/bash 
declare -a filenms=(
    "un"
    "deux"
    "trois"
    "quatre"
    "cinq"
    "six"
    "sept"
    "huit"
    "neuf"
    "dix"
)

for filename in "${filenms[@]}"; do
    [[ $filename == "un" ]] && echo "premième" > ${filename}.doc
    echo "${filename}ième" > ${filename}.doc
done
