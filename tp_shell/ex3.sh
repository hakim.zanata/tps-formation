#!/usr/bin/bash

PASSWD_LOCATION=/etc/passwd
USERS=$@
if [[ $# -eq 0 ]]; then 
    echo "UTILISATION: $0 utilisateur"
    exit 1
fi

for name in "$@"
do
  grep "^$name" /etc/passwd | awk -F: '{print $1,$6,$7}'
done