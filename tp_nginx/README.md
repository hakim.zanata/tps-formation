# TP nginx

# Installation

l'installation de nginx est simple on commance par faire une mise a jours du cache de dnf et apres installer directement par :

```console
[root@localhost nginx]# sudo dnf in -y nginx 
Failed to set locale, defaulting to C.UTF-8
Last metadata expiration check: 1:22:20 ago on Thu Sep 29 14:54:02 2022.
Package nginx-1:1.14.1-9.module_el8.0.0+184+e34fea82.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
[root@localhost nginx]#
```

## configuration virtual host HTTP et HTTPS

```console
upstream loading {
	server 192.168.245.173:7003;
	server 192.168.245.173:7004;
}
server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  tp.com;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.

        location / {
	   proxy_pass http://loading/benefits/;
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
[root@localhost nginx]# sudo setsebool httpd_can_network_connect 1 -P
[root@localhost nginx]# firewall-cmd --add-port=80/tcp --permanent --zone=public
[root@localhost nginx]# firewall-cmd --reload
[root@localhost nginx]# curl -I localhost
HTTP/1.1 200 OK
Server: nginx/1.14.1
Date: Thu, 29 Sep 2022 20:20:28 GMT
Content-Type: text/html
Content-Length: 5832
Connection: keep-alive
Last-Modified: Tue, 21 Feb 2012 18:31:21 GMT
X-ORACLE-DMS-ECID: f2c5770e-f03d-4d92-9810-78e329d18d07-000000d0
X-ORACLE-DMS-RID: 0

[root@localhost nginx]# 
```
donc le serveur http marche bien 
#### HTTPS

dons le cas de https on commece par les certificats ssl :
```console
[root@2a02-8440-3240-d1c5-fb9b-f573-fb58-11f1 conf.d]# sudo chmod 700 /etc/ssl/private 
[root@2a02-8440-3240-d1c5-fb9b-f573-fb58-11f1 conf.d]# #accessible juste par root
[root@2a02-8440-3240-d1c5-fb9b-f573-fb58-11f1 conf.d]# sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt
Generating a RSA private key
.......................+++++
....................................+++++
writing new private key to '/etc/ssl/private/nginx-selfsigned.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:
State or Province Name (full name) []:
Locality Name (eg, city) [Default City]:
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:192.168.245.26   
Email Address []:
[root@2a02-8440-3240-d1c5-fb9b-f573-fb58-11f1 conf.d]# sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
Generating DH parameters, 2048 bit long safe prime, generator 2
This is going to take a long time
[root@2a02-8440-3240-d1c5-fb9b-f573-fb58-11f1 conf.d]# curl https://localhost --insecure
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=us-ascii">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 11">
<meta name=Originator content="Microsoft Word 11">
<link rel=File-List href="welcome_files/filelist.xml">
<link rel=Edit-Time-Data href="welcome_files/editdata.mso">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<title>Dizzyworld Benefits</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>ckarki</o:Author>
  <o:LastAuthor>ckarki</o:LastAuthor>
  <o:Revision>4</o:Revision>
  <o:TotalTime>2</o:TotalTime>
  <o:Created>2006-04-28T17:57:00Z</o:Created>
  <o:LastSaved>2006-04-28T18:00:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Words>62</o:Words>
  <o:Characters>358</o:Characters>
  <o:Company>BEA</o:Company>
  <o:Lines>2</o:Lines>
  <o:Paragraphs>1</o:Paragraphs>
  <o:CharactersWithSpaces>419</o:CharactersWithSpaces>
  <o:Version>11.5703</o:Version>
 </o:DocumentProperties>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:FormsDesign/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
 </w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" LatentStyleCount="156">
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p
	{mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
@page Section1
	{size:8.5in 11.0in;
	margin:1.0in 1.25in 1.0in 1.25in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-parent:"";
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-para-margin:0in;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman";
	mso-ansi-language:#0400;
	mso-fareast-language:#0400;
	mso-bidi-language:#0400;}
</style>
<![endif]-->
</head>

<body lang=EN-US style='tab-interval:.5in'>

<div class=Section1>

<p class=MsoNormal align=center style='text-align:center'><span
style='font-size:24.0pt;color:navy'>Welcome To <span class=SpellE>Dizzyworld</span></span>
</p>

<p align=center style='text-align:center'><span style='font-size:13.5pt;
color:navy'>Select What Benefits You Would Like To See</span> <o:p></o:p></p>

<form action="./servlet" method=POST enctype="application/x-www-form-urlencoded"
NAME=BenefitsForm>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=3 cellpadding=0 width="60%"
 style='width:60.0%;mso-cellspacing:1.5pt;background:yellow;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width="45%" style='width:45.0%;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=right style='text-align:right'><INPUT TYPE="checkbox" NAME="benefit" VALUE="vacation"></p>
  </td>
  <td width="55%" style='width:55.0%;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal>View Vacation Schedule</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td style='padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=right style='text-align:right'><INPUT TYPE="checkbox" NAME="benefit" VALUE="health"></p>
  </td>
  <td style='padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal>View Health Care Options</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td style='padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=right style='text-align:right'><INPUT TYPE="checkbox" NAME="benefit" VALUE="vision"></p>
  </td>
  <td style='padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal>View Vision Options</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3'>
  <td style='padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=right style='text-align:right'><INPUT TYPE="checkbox" NAME="benefit" VALUE="dental"></p>
  </td>
  <td style='padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal>View Dental Options</p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><INPUT TYPE="submit" ACTION="./servlet" VALUE="Get Information" METHOD="POST" NAME="btnSubmit"
  ACTION="./servlet" METHOD=POST></p>
  </td>
 </tr>
</table>

</div>

</form>

<div class=MsoNormal align=center style='text-align:center'>

<hr size=3 width="100%" align=center>

</div>

<p class=MsoNormal>Copyright <span class=SpellE>Dizzyworld</span> 2006 </p>

</div>

</body>

</html>

[root@2a02-8440-3240-d1c5-fb9b-f573-fb58-11f1 conf.d]# cat ssl.conf 
server {
    listen 443 http2 ssl;
    listen [::]:443 http2 ssl;

    server_name tp.com;

    ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
    ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;
    ssl_dhparam /etc/ssl/certs/dhparam.pem;
        location / {
           proxy_pass http://loading/benefits/;
        }

}
[root@2a02-8440-3240-d1c5-fb9b-f573-fb58-11f1 conf.d]# firewall-cmd --add-port=443/tcp --permanent --zone=public
success
[root@2a02-8440-3240-d1c5-fb9b-f573-fb58-11f1 conf.d]# firewall-cmd --reload
[root@2a02-8440-3240-d1c5-fb9b-f573-fb58-11f1 conf.d]# 

```
